# Importing the required modules
import os
import requests
import pandas as pd
from urllib.parse import urlsplit
import openpyxl

# Function to create folders
def create_folders(folder_name):
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)

# Function to download and save image
def download_and_save_image(url, folder_name, image_name):
    try:
        response = requests.get(url)
        file_path = os.path.join(folder_name, f"{image_name}.png")  # Setting the file extension to .png
        with open(file_path, 'wb') as f:
            f.write(response.content)
    except Exception as e:
        print(f"An error occurred: {e}")

# Main function
def main():
    # Define the Excel file and sheet name
    excel_path = 'H2MEET_BoothImage_Automation.xlsx'
    sheet_name = 'Link'
    
    # Read the Excel file into a DataFrame
    df = pd.read_excel(excel_path, sheet_name=sheet_name)

    # Open the Excel file using openpyxl to access hyperlink
    wb = openpyxl.load_workbook(excel_path)
    ws = wb[sheet_name]

    # Create a main folder
    create_folders("Downloaded_Images")

    # Loop through each row in the DataFrame
    for index, row in df.iterrows():
        folder_num = row['Idx']
        sub_folder = os.path.join("Downloaded_Images", f'iFolder{folder_num}')
        create_folders(sub_folder)
        
        # Loop through 'C', 'D', 'E' columns to download images
        for col_idx, col_letter in enumerate(['Img01', 'Img02', 'Img03'], start=3):
            cell = ws.cell(row=index+2, column=col_idx)  # index+2 because DataFrame is 0-indexed and Excel is 1-indexed, and the header row

            if cell.hyperlink:
                url = cell.hyperlink.target
            else:
                continue

            base_url = urlsplit(url)
            image_name = os.path.basename(cell.value)

            download_and_save_image(url, sub_folder, f"{index+1}_{image_name}")

# Execute the main function
if __name__ == "__main__":
    main()
