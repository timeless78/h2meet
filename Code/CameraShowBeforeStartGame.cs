using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using FunnyWorld.Managers;
using FunnyWorld.UtilitiesClient;
using FunnyWorld.UI;

/// <summary>
/// 전시관1씬 한정 (시작전 카메라쇼 클래스)
/// </summary>
public class CameraShowBeforeStartGame : MonoBehaviour
{
    [SerializeField] private StartupManager startupManager;
    [SerializeField] private FadeController fadeController;
    [SerializeField] private GameObject cloud;
    [SerializeField] private GameObject cameraThatWasUsedCameraShow;    //카메라쇼에 사용된 카메라

    void Start()
    {
        fadeController.FadeOut(0.5f, null, true);
        StaticMainCanvas.Instance.SetTitleText("common_4");
        StaticMainCanvas.Instance.ActiveMockUpUi(false);
        StaticMainCanvas.Instance.ActiveCommonButtonUi(false);
        SoundManager.Instance.PlayBgm(1);
    }

    void Update()
    {
        
    }

    public void OnEndAnimation()
    {
        fadeController.FadeIn(0.5f, () =>
        {
            cameraThatWasUsedCameraShow.SetActive(false);
            cloud.SetActive(false);
            startupManager.tmpListener.enabled = true;
            startupManager.StartGame();
        });
    }
}
